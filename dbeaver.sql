1.         Вывести список самолетов с кодами 320, 321, 733;
SELECT *
FROM lanit.AIRCRAFTS_DATA ad 
WHERE AIRCRAFT_CODE IN ('320','321', '733')

SELECT *
FROM lanit.AIRCRAFTS_DATA ad 
WHERE AIRCRAFT_CODE = '320' OR AIRCRAFT_CODE = '321'OR AIRCRAFT_CODE = '733'  

2.         Вывести список самолетов с кодом не на 3;
SELECT *
FROM lanit.AIRCRAFTS_DATA ad 
WHERE AIRCRAFT_CODE NOT LIKE '3%'
3.         Найти билеты оформленные на имя «OLGA», с емайлом «OLGA» или без емайла;
SELECT *
FROM LANIT.TICKETS t  
WHERE PASSENGER_NAME LIKE '%OLGA%' AND (EMAIL LIKE '%olga%' OR  EMAIL IS NULL)
4.         Найти самолеты с дальностью полета 5600, 5700. Отсортировать список по убыванию дальности полета;
SELECT *
FROM LANIT.AIRCRAFTS_DATA ad 
WHERE ad."RANGE"  = 5600 OR ad."RANGE" = 5700
ORDER BY "RANGE"  DESC 
5.         Найти аэропорты в Moscow. Вывести название аэропорта вместе с городом. Отсортировать по полученному названию;
SELECT *
FROM lanit.AIRPORTS_DATA ad 
WHERE ad.CITY = 'Moscow'
ORDER BY ad.AIRPORT_NAME  ASC  

6.         Вывести список всех городов без повторов в зоне «Europe»;
SELECT DISTINCT  CITY 
FROM lanit.AIRPORTS_DATA ad 
WHERE ad.TIMEZONE LIKE '%Europe%'

7.         Найти бронирование с кодом на «3A4» и вывести сумму брони со скидкой 10%
SELECT DISTINCT  BOOK_REF , (TOTAL_AMOUNT*0.9)
FROM lanit.BOOKINGS b 
WHERE b.BOOK_REF LIKE '3A4%'


8.         Вывести все данные по местам в самолете с кодом 320 и классом «Business» строками вида «Данные по месту: номер места 1», «Данные по месту: номер места 2» … и тд
SELECT ad.MODEL, s.FARE_CONDITIONS  , s.SEAT_NO 
FROM LANIT.AIRCRAFTS_DATA ad 
INNER JOIN LANIT.SEATS s ON ad.AIRCRAFT_CODE = s.AIRCRAFT_CODE   WHERE s.AIRCRAFT_CODE = '320' AND s.FARE_CONDITIONS ='Business'


9.         Найти максимальную и минимальную сумму бронирования в 2017 году;
SELECT max(TOTAL_AMOUNT), min (TOTAL_AMOUNT)
FROM LANIT.BOOKINGS b 
10.      Найти количество мест во всех самолетах, вывести в разрезе самолетов;
SELECT DISTINCT  ad.AIRCRAFT_CODE, count(s.AIRCRAFT_CODE)
FROM LANIT.AIRCRAFTS_DATA ad
INNER JOIN LANIT.SEATS s ON ad.AIRCRAFT_CODE = s.AIRCRAFT_CODE
GROUP BY ad.AIRCRAFT_CODE

11.      Найти количество мест во всех самолетах с учетом типа места, вывести в разрезе самолетов и типа мест;
SELECT DISTINCT  ad.AIRCRAFT_CODE, s.FARE_CONDITIONS ,count(s.AIRCRAFT_CODE)
FROM LANIT.AIRCRAFTS_DATA ad
INNER JOIN LANIT.SEATS s ON ad.AIRCRAFT_CODE = s.AIRCRAFT_CODE
GROUP BY ad.AIRCRAFT_CODE, s.FARE_CONDITIONS 

12.      Найти количество билетов пассажира ALEKSANDR STEPANOV, телефон которого заканчивается на 11;
SELECT PASSENGER_NAME ,COUNT(t.TICKET_NO)
FROM LANIT.TICKETS t 
WHERE PASSENGER_NAME = 'ALEKSANDR STEPANOV' AND PHONE LIKE '%11'
GROUP BY PASSENGER_NAME 


13.      Вывести всех пассажиров с именем ALEKSANDR, у которых количество билетов больше 2000. Отсортировать по убыванию количества билетов;
SELECT PASSENGER_NAME,count(TICKET_NO) 
FROM LANIT.TICKETS t 
WHERE PASSENGER_NAME LIKE 'ALEKSANDR %' 
GROUP BY PASSENGER_NAME 
HAVING count(TICKET_NO) >2000
ORDER BY count(TICKET_NO) DESC 

14.      Вывести дни в сентябре 2017 с количеством рейсов больше 500.
SELECT EXTRACT(DAY FROM TRUNC(F.DATE_DEPARTURE)), COUNT(*) 
FROM LANIT.FLIGHTS F
WHERE TRUNC(F.DATE_DEPARTURE, 'MM')=TO_DATE('01.09.2017','DD.MM.YYYY')
GROUP BY TRUNC(F.DATE_DEPARTURE)
HAVING COUNT(*)>500
ORDER BY EXTRACT(DAY FROM TRUNC(F.DATE_DEPARTURE))

15.      Вывести список городов, в которых несколько аэропортов
SELECT CITY,count(CITY)
FROM LANIT.AIRPORTS_DATA ad 
GROUP BY CITY 
HAVING count(CITY)>= 2
ORDER BY city 

16.      Вывести модель самолета и список мест в нем, т.е. на самолет одна строка результата

17.      Вывести информацию по всем рейсам из аэропортов в г.Москва за сентябрь 2017

18.      Вывести кол-во рейсов по каждому аэропорту в г.Москва за 2017

19.      Вывести кол-во рейсов по каждому аэропорту, месяцу в г.Москва за 2017

20.      Найти все билеты по бронированию на «3A4B»
SELECT t.TICKET_NO ,b.BOOK_REF 
FROM LANIT.TICKETS t     
INNER JOIN LANIT.BOOKINGS b 	
ON t.BOOK_REF = b.BOOK_REF AND b.BOOK_REF LIKE '3A4B%'
 

21.      Найти все перелеты по бронированию на «3A4B»

SELECT tf.TICKET_NO,tf.FLIGHT_ID  
FROM LANIT.TICKETS t     
INNER JOIN LANIT.BOOKINGS b 
	ON t.BOOK_REF = b.BOOK_REF AND b.BOOK_REF LIKE '3A4B%' 
INNER JOIN LANIT.TICKET_FLIGHTS tf 
	ON t.TICKET_NO = tf.TICKET_NO 