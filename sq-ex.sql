//////Задание: 1 (Serge I: 2002-09-30) //////////
Найдите номер модели, 
скорость и размер жесткого диска для всех ПК стоимостью менее 500 дол. 
Вывести: model, speed и hd

SELECT model, speed, hd 
FROM  pc 
Where  pc.price < 500

//////////////////////////////////////////////////////////////////////
Задание: 2 (Serge I: 2002-09-21)
Найдите производителей принтеров. Вывести: maker

SELECT DISTINCT MAKER
FROM PRODUCT
WHERE PRODUCT.TYPE = 'PRINTER'
//////////////////////////////////////////////////////////////////////
Задание: 3 (Serge I: 2002-09-30)
Найдите номер модели, объем памяти и размеры экранов ПК-блокнотов, цена которых превышает 1000 дол.

select model , ram,screen
from laptop
where laptop.price > 1000

//////////////////////////////////////////////////////////////////////
Задание: 4 (Serge I: 2002-09-21)
Найдите все записи таблицы Printer для цветных принтеров.

select *
from Printer
where color ='y'

//////////////////////////////////////////////////////////////////////
Задание: 5 (Serge I: 2002-09-30)
Найдите номер модели, скорость и размер жесткого диска ПК, имеющих 12x или 24x CD и цену менее 600 дол.

SELECT MODEL, SPEED, HD 
FROM PC 
WHERE PC.CD IN ('12X', '24X') AND PRICE < 600

//////////////////////////////////////////////////////////////////////
Задание: 6 (Serge I: 2002-10-28)
Для каждого производителя, выпускающего ПК-блокноты c объёмом жесткого диска не менее 10 Гбайт, найти скорости таких ПК-блокнотов. Вывод: производитель, скорость.

SELECT  MAKER , SPEED 
FROM LAPTOP
INNER JOIN PRODUCT ON PRODUCT.MODEL = LAPTOP.MODEL  
WHERE LAPTOP.HD>=10
GROUP BY MAKER , SPEED


//////////////////////////////////////////////////////////////////////
Задание: 7 (Serge I: 2002-11-02)
Найдите номера моделей и цены всех имеющихся в продаже продуктов (любого типа) производителя B (латинская буква).

SELECT PRODUCT.MODEL, PC.PRICE
FROM PRODUCT INNER JOIN PC ON PRODUCT.MODEL = PC.MODEL WHERE MAKER = 'B'
UNION
SELECT PRODUCT.MODEL, LAPTOP.PRICE
FROM PRODUCT INNER JOIN LAPTOP ON PRODUCT.MODEL=LAPTOP.MODEL WHERE MAKER ='B'
UNION
SELECT PRODUCT.MODEL, PRINTER.PRICE
FROM PRODUCT INNER JOIN PRINTER ON PRODUCT.MODEL=PRINTER.MODEL WHERE MAKER ='B'

//////////////////////////////////////////////////////////////////////
Задание: 8 (Serge I: 2003-02-03)
Найдите производителя, выпускающего ПК, но не ПК-блокноты.
SELECT MAKER
FROM PRODUCT
WHERE TYPE = 'PC'
EXCEPT
SELECT MAKER
FROM PRODUCT
WHERE TYPE = 'LAPTOP'

//////////////////////////////////////////////////////////////////////

Задание: 9 (Serge I: 2002-11-02)
Найдите производителей ПК с процессором не менее 450 Мгц. Вывести: Maker
SELECT MAKER
FROM PC
INNER JOIN PRODUCT ON PC.MODEL = PRODUCT.MODEL 
WHERE PC.SPEED>= 450
GROUP BY MAKER

//////////////////////////////////////////////////////////////////////
Задание: 10 (Serge I: 2002-09-23)
Найдите модели принтеров, имеющих самую высокую цену. Вывести: model, price

SELECT MODEL, PRICE
FROM PRINTER
WHERE PRICE =
(SELECT MAX(PRICE) FROM PRINTER )
//////////////////////////////////////////////////////////////////////

Задание: 11 (Serge I: 2002-11-02)
Найдите среднюю скорость ПК.
SELECT AVG(SPEED)
FROM PC

//////////////////////////////////////////////////////////////////////
Задание: 12 (Serge I: 2002-11-02)
Найдите среднюю скорость ПК-блокнотов, цена которых превышает 1000 дол.
SELECT AVG(SPEED)
FROM LAPTOP
WHERE LAPTOP.PRICE > 1000

//////////////////////////////////////////////////////////////////////
Задание: 13 (Serge I: 2002-11-02)
Найдите среднюю скорость ПК, выпущенных производителем A.
SELECT AVG(SPEED)
FROM PC, PRODUCT
WHERE PC.MODEL = PRODUCT.MODEL AND PRODUCT.MAKER = 'A'

//////////////////////////////////////////////////////////////////////
Задание: 14 (Serge I: 2002-11-05)
Найдите класс, имя и страну для кораблей из таблицы Ships, имеющих не менее 10 орудий.

SELECT SHIPS.CLASS, SHIPS.NAME, CLASSES.COUNTRY
FROM SHIPS 
LEFT JOIN CLASSES ON SHIPS.CLASS = CLASSES.CLASS
WHERE CLASSES.NUMGUNS >= 10

//////////////////////////////////////////////////////////////////////
Задание: 15 (Serge I: 2003-02-03)
Найдите размеры жестких дисков, совпадающих у двух и более PC. Вывести: HD
SELECT HD 
FROM PC 
GROUP BY HD 
HAVING COUNT(MODEL) >= 2
///////////////////////////////////////////////////////////////////////
Задание: 16 (Serge I: 2003-02-03)
Найдите пары моделей PC, имеющих одинаковые скорость и RAM. В результате каждая пара указывается только один раз, т.е. (i,j), но не (j,i), Порядок вывода: модель с большим номером, модель с меньшим номером, скорость и RAM.
SELECT DISTINCT PC1.MODEL, PC2.MODEL, PC1.SPEED,PC1.RAM
FROM PC PC1, PC PC2
WHERE PC1.SPEED = PC2.SPEED AND PC1.RAM = PC2.RAM AND PC1.MODEL > PC2.MODEL
///////////////////////////////////////////////////////////////////////

Задание: 17 (Serge I: 2003-02-03)
Найдите модели ПК-блокнотов, скорость которых меньше скорости каждого из ПК.
Вывести: type, model, speed

SELECT DISTINCT P.TYPE,P.MODEL,L.SPEED
FROM LAPTOP L
JOIN PRODUCT P ON L.MODEL = P.MODEL
WHERE L.SPEED <(SELECT MIN(SPEED)FROM PC)
///////////////////////////////////////////////////////////////////////

Задание: 18 (Serge I: 2003-02-03)
Найдите производителей самых дешевых цветных принтеров. Вывести: maker, price
SELECT DISTINCT PRODUCT.MAKER, PRINTER.PRICE
FROM PRODUCT, PRINTER
WHERE PRODUCT.MODEL = PRINTER.MODEL
AND PRINTER.COLOR = 'Y'
AND PRINTER.PRICE = (SELECT MIN(PRICE) 
FROM PRINTER
WHERE PRINTER.COLOR = 'Y')

///////////////////////////////////////////////////////////////////////

Задание: 19 (Serge I: 2003-02-13)
Для каждого производителя, имеющего модели в таблице Laptop, найдите средний размер экрана выпускаемых им ПК-блокнотов.
Вывести: maker, средний размер экрана.
SELECT PRODUCT.MAKER, AVG(SCREEN)
FROM LAPTOP
LEFT JOIN PRODUCT ON PRODUCT.MODEL = LAPTOP.MODEL
GROUP BY PRODUCT.MAKER

///////////////////////////////////////////////////////////////////////
Задание: 20 (Serge I: 2003-02-13)
Найдите производителей, выпускающих по меньшей мере три различных модели ПК. Вывести: Maker, число моделей ПК.

SELECT maker, COUNT(model)
FROM product
WHERE type = 'pc'
GROUP BY product.maker
HAVING COUNT (DISTINCT model) >= 3

///////////////////////////////////////////////////////////////////////
Задание: 21 (Serge I: 2003-02-13)
Найдите максимальную цену ПК, выпускаемых каждым производителем, у которого есть модели в таблице PC.
Вывести: maker, максимальная цена.

SELECT PRODUCT.MAKER, MAX(PC.PRICE)
FROM PRODUCT, PC
WHERE PRODUCT.MODEL = PC.MODEL
GROUP BY PRODUCT.MAKER